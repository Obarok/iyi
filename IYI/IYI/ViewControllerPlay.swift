//
//  ViewControllerPlay.swift
//  IYI
//
//  Created by Daniel Jørgensen & Mikkel Schmøde on 20/11/2018.
//  Copyright © 2018 MD. All rights reserved.
//

import UIKit
import AVFoundation
import CoreMotion

class ViewControllerPlay: UIViewController {
    
    @IBOutlet var MainLabel: UILabel!
    
    @IBOutlet weak var LeftButtonLabel: UILabel!
    
    @IBOutlet weak var RightButtonLabel: UILabel!
    
    var motionManager = CMMotionManager() // Variable for the gyro
    
    // Sound variables
    let back = Bundle.main.path(forResource: "back.mp3", ofType:nil)! // back sound
    let wrongSound = Bundle.main.path(forResource: "wrongSound.wav", ofType:nil)! // wrong sound
    let correctSound = Bundle.main.path(forResource: "correctSound.wav", ofType:nil)! // correct sound
    var audioPlayer = AVAudioPlayer() // the sound player that plays the sounds
    
    // score variables
    var score : Int = 0 // initialize the score variable
    var count : Int = 0 // initialize the count variable
    var fontSize : CGFloat = 100 // // initialize the fontSize variable where 100 is the default font size, in the beginning of the game.
    
    var readString = "" // String for load methods.
    
    let letters = [ // initialize an array of all characters of the alphabet
        "A", "B", "C", "D", "E", "F", "G",
        "H", "I", "J", "K", "L", "M", "N",
        "O", "P", "Q", "R", "S", "T", "U",
        "V", "W", "X", "Y", "Z"
    ]
    
    let leftOrRight = [1, 2] // initialize an array where 1 is left, and 2 is right.
    
    var correct = 0 // initialize a variable, that controls the side for the correct answer, 1 is left and 2 is right.
    
    var abc:String = "" // initialize the string, when the correct 3 letters
    var def:String = "" // initialize the string, with the wrong 3 letters.
    
    // Function that loads, when the view is loaded.
    override func viewDidLoad() {
        super.viewDidLoad()
        randLetters()
        setLetters()
        MainLabel.font = MainLabel.font.withSize(self.fontSize)
        self.fontSize = fontSize - 10
        gyro()
    }
    
    // The function of the back button, this will always get to the Menu view from the ViewControllerMenu.
    @IBAction func Back(_ sender: Any) {
        motionManager.stopGyroUpdates()
        // Loads the sound file
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: back))
            audioPlayer.play() // plays the sound file
        } catch {
            print("couldn't load sound file")
        }
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier:"Menu") as! ViewControllerMenu
        self.present(vc, animated: true, completion: nil)
    }
    
    // The function for the left button.
    @IBAction func LeftButton(_ sender: Any) {
        
        // Checks if this size contains the right answer, if the user presses the button, the user gets the points.
        if LeftButtonLabel.text == abc{
            
            // Loads the sound file
            do {
                audioPlayer = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: correctSound))
                audioPlayer.play() // plays the sound file
            } catch {
                print("couldn't load sound file")
            }
            
            MainLabel.font = MainLabel.font.withSize(self.fontSize)
            self.fontSize = fontSize - 10
            score = score + 1 * count + 1
            
        } else {
            
            // Loads the sound file
            do {
                audioPlayer = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: wrongSound))
                audioPlayer.play() // plays the sound file
            } catch {
                print("couldn't load sound file")
            }
        }
        
        // After the user has pick the answer, it will generate a new set of strings and the count variable will go 1 up.
        randLetters()
        setLetters()
        count = count + 1
        
        // When the count variable gets to 10, the game has finished and the view will change.
        if count == 10{
            ViewChange()
        }
    }
    
    // The function for the right button.
    @IBAction func RightButton(_ sender: Any) {
        
        // Checks if this size contains the right answer, if the user presses the button, the user gets the points.
        if RightButtonLabel.text == abc{
            
            // Loads the sound file
            do {
                audioPlayer = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: correctSound))
                audioPlayer.play() // plays the sound file
            } catch {
                print("couldn't load sound file")
            }
            
            MainLabel.font = MainLabel.font.withSize(self.fontSize)
            self.fontSize = fontSize - 10
            score = score + 1 * count + 1
        } else {
            
            // Loads the sound file
            do {
                audioPlayer = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: wrongSound))
                audioPlayer.play() // plays the sound file
            } catch {
                print("couldn't load sound file")
            }
        }
        
        // After the user has pick the answer, it will generate a new set of strings and the count variable will go 1 up.
        randLetters()
        setLetters()
        count = count + 1
        
        // When the count variable gets to 10, the game has finished and the view will change.
        if count == 10{
            ViewChange()
        }
    }
    
    // This function generates the 3 random letters.
    func randLetters () {
        let a = letters.randomElement()!
        let b = letters.randomElement()!
        let c = letters.randomElement()!
        
        abc = a + b + c // converts the 3 random letters to a string.
        
        let d = letters.randomElement()!
        let e = letters.randomElement()!
        let f = letters.randomElement()!
        
        def = d + e + f // converts the 3 random letters to a string.
        
        correct = leftOrRight.randomElement()! // Makes the correct either 1 or 2.
    }
    
    // This function sets the labels from the two strings, abc and def.
    func setLetters(){
        MainLabel.text = abc // sets the mainLabel to ABC
        
        if correct == 1{ // If the correct is 1, the right answer will be on the left side.
            LeftButtonLabel.text = abc
            RightButtonLabel.text = def
        }
        
        if correct == 2{ // If the correct is 2, the right answer will be on the right side.
            LeftButtonLabel.text = def
            RightButtonLabel.text = abc
        }
    }
    
    // This function changes the view to the done view from the ViewControllerDone.
    func ViewChange(){
        motionManager.stopGyroUpdates()
        compareHighScores()
        
        save(name: "CurrentScore", Pointscore: score)
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "Done") as! ViewControllerDone
        self.present(vc, animated: true, completion: nil)
    }
    
    func save(name:String, Pointscore:Int) {
        // Local variables
        let fileName = name // The name of the file with the current score
        let DocumentDirURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)// The url of the file
        let fileURL = DocumentDirURL.appendingPathComponent(fileName).appendingPathExtension("txt") // The url of the file
        
        let writeScore = String(Pointscore) // Converts the writeScore value to a String that contains the score.
        
        do {
            // writes to file
            try writeScore.write(to: fileURL, atomically: true, encoding: String.Encoding.utf8)
        }
        catch let error as NSError{
            print(error)
        }
    }
    
    func load(name:String){
        let fileName = name // The name of the file with the current score
        let DocumentDirURL = try!  FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true) // The url of the file
        let fileURL = DocumentDirURL.appendingPathComponent(fileName).appendingPathExtension("txt") // Finds the find at the url
        
        do {
            readString = try String(contentsOf: fileURL) // reads the file and saves it in the readString variable
        } catch let error as NSError {
            print(error)
        }
    }
    
    func compareHighScores(){
        // Local variables
        
        load(name: "HighScore")
        
        // Compares the score against the highscore from the file
        var x : Int! = Int(readString) // sets the x variable to an int, from the string.
        
        // If there is no value from the file, insert a 0.
        if x == nil{
            x=0
        }
        
        // If score is bigger than the current highscore, save the new highscore
        if score > x{
            save(name: "HighScore", Pointscore: score)
        }
    }
    
    func gyro() {
        motionManager.gyroUpdateInterval = 0.15 // Default value 0.15
        motionManager.startGyroUpdates(to: OperationQueue.current!) { (data, error) in
            if let myData = data{
                
                if (UIDevice.current.orientation == UIDeviceOrientation.landscapeRight){ // checks the orientation of the iphone
                    
                    if myData.rotationRate.x > 3{ // Default value 3
                        let button = UIButton()
                        self.LeftButton(button)
                    }
                    if myData.rotationRate.x < -3 { // Default value 3
                        let button = UIButton()
                        self.RightButton(button)
                    }
                }
                
                if (UIDevice.current.orientation == UIDeviceOrientation.landscapeLeft){ // checks the orientation of the iphone
                    
                    if myData.rotationRate.x > 3{ // Default value 3
                        let button = UIButton()
                        self.RightButton(button)
                    }
                    if myData.rotationRate.x < -3 { // Default value 3
                        let button = UIButton()
                        self.LeftButton(button)
                    }
                }
            }
        }
    }
}
