//
//  ViewController.swift
//  IYI
//
//  Created by Daniel Jørgensen & Mikkel Schmøde on 20/11/2018.
//  Copyright © 2018 MD. All rights reserved.
//

import UIKit
import AVFoundation

class ViewControllerMenu: UIViewController {
    
    let forward = Bundle.main.path(forResource: "forward.mp3", ofType:nil)! // back sound
    var audioPlayer = AVAudioPlayer() // the sound player that plays the sounds
    
    // uses the super constructor
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // uses the super constructor
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    // The how to play button
    @IBAction func HowToplay(_ sender: Any) {
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: forward))
            audioPlayer.play() // plays the sound file
        } catch {
            print("couldn't load sound file")
        }
    }
    
    // The Highscore button
    @IBAction func HighScore(_ sender: Any) {
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: forward))
            audioPlayer.play() // plays the sound file
        } catch {
            print("couldn't load sound file")
        }
    }
    
    // The play button
    @IBAction func Play(_ sender: Any) {
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: forward))
            audioPlayer.play() // plays the sound file
        } catch {
            print("couldn't load sound file")
        }
    }
    
}
