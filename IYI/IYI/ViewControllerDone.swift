//
//  ViewControllerDone.swift
//  IYI
//
//  Created by Daniel Jørgensen & Mikkel Schmøde on 20/11/2018.
//  Copyright © 2018 MD. All rights reserved.
//

import UIKit
import AVFoundation

class ViewControllerDone: UIViewController {
    
    // The label that tells the score
    @IBOutlet weak var ScoreLabel: UILabel!
    
    let back = Bundle.main.path(forResource: "back.mp3", ofType:nil)! // back sound
    let forward = Bundle.main.path(forResource: "forward.mp3", ofType:nil)! // back sound
    var audioPlayer = AVAudioPlayer() // the sound player that plays the sounds
    
    // Function that loads the current score of the game, when the view has been loaded.
    override func viewDidLoad() {
        loadScore()
    }
    
    // Function of the play again button that goes to the ViewControllerPlay
    @IBAction func PlayAgain(_ sender: Any) {
        // Loads the sound file
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: forward))
            audioPlayer.play() // plays the sound file
        } catch {
            print("couldn't load sound file")
        }
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier:"Play") as! ViewControllerPlay // Changes to the play viewcontroller
        self.present(vc, animated: true, completion: nil)
    }
    
    // Function of the Menu button, that goes goes to the ViewControllerMenu
    @IBAction func Menu(_ sender: Any) {
        // Loads the sound file
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: back))
            audioPlayer.play() // plays the sound file
        } catch {
            print("couldn't load sound file")
        }
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier:"Menu") as! ViewControllerMenu // Changes to the menu viewcontroller
        self.present(vc, animated: true, completion: nil)
    }
    
    //Loads the current score
    func loadScore(){
        let fileName = "CurrentScore" // The name of the file with the current score
        var readString = "" // initialize varaible where the data will be stored
        let DocumentDirURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true) // The url of the file
        let fileURL = DocumentDirURL.appendingPathComponent(fileName).appendingPathExtension("txt") // Finds the find at the url
        
        do {
            readString = try String(contentsOf: fileURL) // reads the file and saves it in the readString variable
        } catch let error as NSError {
            print("Failed to load file")
            print(error)
        }
        ScoreLabel.text = readString // sets the label from the data in the string.
    }

}
